import React, { HTMLAttributes } from "react";

// interface Props {
//   items: string[];
//   style?: "list-none" | "list-disc" | "list-decimal";
//   color?: string;
// }

export type Props = HTMLAttributes<HTMLLIElement> & {
  items: string[];
};

const List = ({ items, className }: Props) => {
  return (
    <ul className={`pl-5 text-left list-disc ${className}`}>
      {items.map(item => {
        return (
          <li key={item} className={`text-slate-500`}>
            {item}
          </li>
        );
      })}
    </ul>
  );
};

export default List;
