import React, { HTMLAttributes } from "react";

type Props = HTMLAttributes<HTMLDivElement> & {
  title: string;
  description?: string;
};

const SectionWrapper = ({ title, className, children, description }: Props) => {
  return (
    <>
      <div className={`light-gradient-bg pt-[4.5rem] pb-[3.25rem] ${className}`}>
        <h2 className="uppercase text-[2.375rem] text-center font-black">{title}</h2>
        <p className="text-center text-lg leading-5 font-medium mb-[1rem] md:mb-[4.2rem] text-lr-gray">{description}</p>
        {children}
      </div>
    </>
  );
};

export default SectionWrapper;
