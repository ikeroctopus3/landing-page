import React, { ButtonHTMLAttributes } from "react";

export type Props = ButtonHTMLAttributes<HTMLButtonElement> & {
  children: React.ReactNode; // Not required ...
  //className?: "string"
};

const SecondaryButton = ({ children, className, onClick }: Props) => {
  return (
    <>
      <button
        onClick={onClick}
        className={`bg-slate-200 text-lr-blue px-4 py-1 font-medium rounded-lg hover:bg-lr-blue hover:text-white ${className}`}>
        {children}
      </button>
    </>
  );
};

export default SecondaryButton;
