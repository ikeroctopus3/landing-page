import React, { ButtonHTMLAttributes } from "react";

export type Props = ButtonHTMLAttributes<HTMLButtonElement>;

const BtnPrimary = ({ children, className }: Props) => {
  return (
    <button
      className={`min-w-[15rem] lg:min-w-[25.75rem] bg-lr-blue text-white py-3 px-10 rounded-[15px] text-xl hover:bg-lr-darker-blue ${className}`}>
      {children}
    </button>
  );
};

export default BtnPrimary;
