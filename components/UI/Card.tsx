import React, { HTMLAttributes } from "react";

import List from "@/components/UI/List";

export type Props = {
  title: string;
  items: string[];
  src: string;
  listStyle?: "list-none" | "list-disc";
};

const InfoCard = ({ title, items, src, listStyle = "list-disc" }: Props) => {
  return (
    /* 200px */
    <div className="mt-5 md:mt-0 max-w-[16rem] text-center p-5">
      <div className="flex justify-center">
        <img src={src} alt="" width={85} height={85} />
      </div>
      <h3 className="text-xl font-bold text-center my-4">{title}</h3>
      <List items={items} className={listStyle} />
    </div>
  );
};

export default InfoCard;

// How to manage different kind of types and recieve them as params and use them different elments?

// import React from 'react';

// interface DivProps extends React.HTMLAttributes<HTMLDivElement> {
//   // additional props for div element
// }

// interface ImageProps extends React.HTMLAttributes<HTMLImageElement> {
//   // additional props for image element
// }

// type InfoCardProps = DivProps & ImageProps;

// const InfoCard: React.FC<InfoCardProps> = ({ title, items, src }) => {
//   // render InfoCard component
//   return (
//     <div>
//       <h1>{title}</h1>
//       <ul>
//         {items.map((item, index) => (
//           <li key={index}>{item}</li>
//         ))}
//       </ul>
//       <img src={src} alt="InfoCard" />
//     </div>
//   );
// };

// export default InfoCard;
