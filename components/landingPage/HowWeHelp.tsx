import React from "react";

import SectionWrapper from "@/components/UI/SectionWrapper";
import BtnPrimary from "@/components/UI/PrimaryButton";

import InfoCard from "../../components/UI/Card";
// import build from "next/dist/build";

const HowWeHelp = () => {
  const programListItems = ["Forever FREE!", "Complete Programs list", "Countries information", "Residential data"];
  const businessTeamItems = [
    "Business assessment",
    "Team assessment",
    "Review data & docs",
    "Professional consultation",
    "Team-up facilitating",
  ];
  const matchProgramsItems = [
    "Matching by experts",
    "Essential docs",
    "Pitch deck",
    "Business plan",
    "Market research",
    "Financial projections",
    "Safe apply by expert",
  ];

  return (
    <div>
      <SectionWrapper title="how we help">
        <div className="md-m-width m-auto flex justify-around flex-wrap">
          <InfoCard src="/icons/mvp.gif" title="The Most Complete List of Programs" items={programListItems} />
          <InfoCard src="/icons/check-list.gif" title="Business & Team Assessment" items={businessTeamItems} />
          <InfoCard src="/icons/find.gif" title="Match to Programs & Safe Apply" items={matchProgramsItems} />
        </div>
        <div className="text-center mt-16">
          <BtnPrimary>Let's start</BtnPrimary>
        </div>
      </SectionWrapper>
    </div>
  );
};

export default HowWeHelp;
