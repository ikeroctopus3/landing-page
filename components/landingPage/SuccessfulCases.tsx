import React from "react";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, EffectCoverflow, Navigation } from "swiper/modules";

import SectionWrapper from "@/components/UI/SectionWrapper";

const SuccessfulCases = () => {
  return (
    <>
      <SectionWrapper title="our successful cases" className="pb-24">
        <Swiper
          effect={"coverflow"}
          grabCursor={true}
          centeredSlides={true}
          // loop={true}
          initialSlide={1}
          breakpoints={{
            0: {
              slidesPerView: 1,
              spaceBetween: 250,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 100,
            },
            1024: {
              slidesPerView: 3,
              spaceBetween: 200,
            },
          }}
          coverflowEffect={{
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: false,
          }}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination, EffectCoverflow, Navigation]}
          onSlideChange={() => console.log("slide change")}
          onSwiper={swiper => console.log(swiper)}>
          <SwiperSlide className="bg-transparent">
            <div className="flex justify-center p-2">
              <img src="/imgs/case-01.jpg" width={297} height={384} className="lr-box-shadow " />
            </div>
          </SwiperSlide>
          <SwiperSlide className="bg-transparent">
            <div className="flex justify-center p-2">
              <img src="/imgs/case-02.png" width={297} height={384} className="lr-box-shadow " />
            </div>
          </SwiperSlide>
          <SwiperSlide className="bg-transparent">
            <div className="flex justify-center p-2">
              <img src="/imgs/case-03.jpg" width={297} height={384} className="lr-box-shadow " />
            </div>
          </SwiperSlide>
        </Swiper>
      </SectionWrapper>
    </>
  );
};

export default SuccessfulCases;
