import React from "react";

import SectionWrapper from "@/components/UI/SectionWrapper";
import BtnPrimary from "@/components/UI/PrimaryButton";

import InfoCard from "../../components/UI/Card";

const SuitCountry = () => {
  /* XD About Icons, I will fix it soon */
  const items = ["❌ Work Permit", "✔ Fast Visa", "✔ Low Cost of Living", "❌ Allow Business Pivot"];

  return (
    <>
      <SectionWrapper title="Which country suits you best?">
        <div className="md-m-width m-auto flex justify-around flex-wrap">
          <InfoCard items={items} title="United Kingdom" src="/imgs/united-kingdom.png" listStyle="list-none" />
          <InfoCard items={items} title="Canada" src="/imgs/canada.png" listStyle="list-none" />
          <InfoCard items={items} title="Netherland" src="/imgs/netherland.png" listStyle="list-none" />
        </div>
        <div className="text-center mt-16">
          <BtnPrimary>See the Visa Startup Guide</BtnPrimary>
        </div>
      </SectionWrapper>
    </>
  );
};

export default SuitCountry;
