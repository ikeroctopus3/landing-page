import React from "react";

import SectionWrapper from "@/components/UI/SectionWrapper";

import PrimaryButton from "@/components/UI/PrimaryButton";

const ProgramsList = () => {
  const counter = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  return (
    <>
      <SectionWrapper title="programs list" description="The Most Complete List of Programs" className="relative">
        <div className="flex flex-wrap justify-center md-m-width m-auto max-h-[75vh] overflow-hidden mt-12 md:mt-0">
          {counter.map(number => {
            return (
              <div className="relative basis-1/4 px-2 pb-3 " key={number}>
                <div className="text-center bg-white rounded-lg p-2 border min-w-[18rem] md:min-w-[14rem] min-h-[20rem] shadow-sm">
                  <img src="/imgs/canada.png" className="m-auto mt-3" alt="logo company" width={50} height={50} />
                  <h1 className="text-2xl font-semibold my-3 leading-6">Future Founder FellowShip</h1>
                  <p className="my-2">Lorem ipsum dolor sit amet.</p>
                  <p className="text-sm leading-4 text-lr-gray">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam qui eius eveniet fuga iste porro
                    facilis rem impedit fugit labore.
                  </p>
                  <span className="absolute left-4 bottom-4">
                    <img src="/icons/logo.png" alt="logo" className="rounded-full" width={45} height={45} />
                  </span>
                </div>
              </div>
            );
          })}
        </div>
        <div className="light-tp-gradient w-full h-44 absolute bottom-0 left-0 flex justify-center items-center">
          <PrimaryButton>See The Complete List</PrimaryButton>
        </div>
      </SectionWrapper>
    </>
  );
};

export default ProgramsList;
