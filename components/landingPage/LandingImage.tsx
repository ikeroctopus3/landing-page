import React from "react";

import PrimaryButton from "@/components/UI/PrimaryButton";

const LandingImage = () => {
  return (
    <>
      <section>
        {/* Used background Image: tailwind.config.js -> backgroundImage */}
        <div className="bg-landing-image bg-cover bg-bottom min-h-[110vh] mt-[-54px]">
          <div className="m-auto lg-m-width flex items-center min-h-[90vh]">
            <div className="text-white  block">
              <h1 className="text-3xl font-bold lg:text-5xl lg:font-black lg:leading-[3.25rem] text-shadow">
                Discover Your Ideal <br /> Startup Program in +500 <br /> Opportunities Worldwide
              </h1>
              <p className="text-lg my-2 text-shadow font-medium">
                We connect <span>startups</span> to <span>programs</span> of incubators, VCs, and more.
              </p>
              <PrimaryButton className="py-4 font-mediudm">Find my match program &gt;</PrimaryButton>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default LandingImage;
