import React from "react";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, EffectCoverflow, Navigation, Autoplay } from "swiper/modules";

import SectionWrapper from "@/components/UI/SectionWrapper";

const CustomerFeedback = () => {
  return (
    <>
      <SectionWrapper title="from our customers" className="pb-28">
        <Swiper
          className="!pb-28 !pt-10"
          effect={"coverflow"}
          grabCursor={true}
          centeredSlides={true}
          // loop={true}
          initialSlide={1}
          autoplay={{
            delay: 2500,
            pauseOnMouseEnter: true,
          }}
          breakpoints={{
            0: {
              slidesPerView: 1.1,
              spaceBetween: 50,
            },
            768: {
              slidesPerView: 1.5,
              spaceBetween: 150,
            },
            1024: {
              slidesPerView: 1.5,
              spaceBetween: 170,
            },
            1280: {
              slidesPerView: 1.8,
              spaceBetween: 170,
            },
          }}
          coverflowEffect={{
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: false,
          }}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination, EffectCoverflow, Navigation, Autoplay]}>
          <SwiperSlide className="bg-white rounded-xl shadow-lr-bs-2">
            <div className="bg-white rounded-xl lg:flex">
              <img
                src="/imgs/customer-2.png"
                alt="customer photo"
                width={220}
                height={220}
                className="rounded-xl mt-3 lg:mt-0 lg:rounded-e-none bg-blue-100 m-auto"
              />
              <div>
                <img
                  src="/icons/quote.png"
                  alt="quote photo"
                  width={57}
                  height={57}
                  className="-mt-12 lg:mt-0 lg:-ml-8 "
                />
                <figure className="ml-6 mr-3">
                  <blockquote className="font-medium text-xl leading-6 mt-4 lg:mt-0 text-lr-txt-gray">
                    I just had an idea and wanted to get a startup visa. The LaunchRoad team did all the work for me
                    like a friend by my side.
                  </blockquote>
                  <figcaption className="font-medium mt-10 pb-4 text-lg md:text-xl leading-6  text-lr-txt-gray">
                    <span className="text-black">Canada - </span>
                    Letter of Support
                    <img src="/icons/success.png" alt="success icon" width={31} height={31} className="inline mx-3" />
                  </figcaption>
                </figure>
              </div>
            </div>
          </SwiperSlide>

          <SwiperSlide className="bg-white rounded-xl shadow-lr-bs-2">
            <div className="bg-white rounded-xl lg:flex">
              <img
                src="/imgs/customer-1.png"
                alt="customer photo"
                width={220}
                height={220}
                className="rounded-xl mt-3 lg:mt-0 lg:rounded-e-none bg-blue-100 m-auto"
              />
              <div>
                <img
                  src="/icons/quote.png"
                  alt="quote photo"
                  width={57}
                  height={57}
                  className="-mt-12 lg:mt-0 lg:-ml-8 "
                />
                <figure className="ml-6 mr-3">
                  <blockquote className="font-medium text-xl leading-6 mt-4 lg:mt-0 text-lr-txt-gray">
                    I just had an idea and wanted to get a startup visa. The LaunchRoad team did all the work for me
                    like a friend by my side.
                  </blockquote>
                  <figcaption className="font-medium mt-10 pb-4 text-lg md:text-xl leading-6  text-lr-txt-gray">
                    <span className="text-black">Canada - </span>
                    Letter of Support
                    <img src="/icons/success.png" alt="success icon" width={31} height={31} className="inline mx-3" />
                  </figcaption>
                </figure>
              </div>
            </div>
          </SwiperSlide>

          <SwiperSlide className="bg-white rounded-xl shadow-lr-bs-2">
            <div className="bg-white rounded-xl lg:flex">
              <img
                src="/imgs/customer-2.png"
                alt="customer photo"
                width={220}
                height={220}
                className="rounded-xl mt-3 lg:mt-0 lg:rounded-e-none bg-blue-100 m-auto"
              />
              <div>
                <img
                  src="/icons/quote.png"
                  alt="quote photo"
                  width={57}
                  height={57}
                  className="-mt-12 lg:mt-0 lg:-ml-8 "
                />
                <figure className="ml-6 mr-3">
                  <blockquote className="font-medium text-xl leading-6 mt-4 lg:mt-0 text-lr-txt-gray">
                    I just had an idea and wanted to get a startup visa. The LaunchRoad team did all the work for me
                    like a friend by my side.
                  </blockquote>
                  <figcaption className="font-medium mt-10 pb-4 text-lg md:text-xl leading-6  text-lr-txt-gray">
                    <span className="text-black">Canada - </span>
                    Letter of Support
                    <img src="/icons/success.png" alt="success icon" width={31} height={31} className="inline mx-3" />
                  </figcaption>
                </figure>
              </div>
            </div>
          </SwiperSlide>
        </Swiper>
      </SectionWrapper>
    </>
  );
};

export default CustomerFeedback;
