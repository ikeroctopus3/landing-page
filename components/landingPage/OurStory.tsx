import React from "react";

import SectionWrapper from "@/components/UI/SectionWrapper";

const OurStory = () => {
  return (
    <>
      <SectionWrapper title="our story" className="text-lr-blue relative">
        <img
          src="/imgs/pattern.png"
          alt="pattern desing"
          className="absolute left-[3rem] top-[4.13rem] md:left-[7.5rem]"
        />
        <p className="text-lr-dark-gray text-[1.75rem] leading-8 font-semibold sm-m-width m-auto text-center mt-28 mb-40">
          When getting a startup visa, we wish someone who had already gone this route had been by our side and told us
          the details.
          <br />
          <br />
          Therefore, we decided to help talents who want to take this path like us to reach their goal with more peace
          of mind and less risk.
        </p>
        <img
          src="/imgs/pattern.png"
          alt="pattern desing"
          className="absolute right-[3rem] md:right-[7.5rem] bottom-[4.44rem]"
        />
      </SectionWrapper>
    </>
  );
};

export default OurStory;
