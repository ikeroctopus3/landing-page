import React from "react";

import SectionWrapper from "@/components/UI/SectionWrapper";

const ApplyStepsGif = () => {
  return (
    <>
      <SectionWrapper title="Apply in 3 simple steps!">
        <div className="p-2 flex justify-center">
          <img
            src="/imgs/ApplyStepsGif.gif"
            alt="Apply Steps Gif"
            className="max-w-full md:max-w-[45rem] aspect-video rounded-[20px]"
          />
        </div>
      </SectionWrapper>
    </>
  );
};

export default ApplyStepsGif;
