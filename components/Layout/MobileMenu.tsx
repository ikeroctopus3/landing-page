import React from "react";

import { BsGlobe } from "react-icons/bs";
import { AiOutlineClose } from "react-icons/ai";

import BtnSecondary from "../UI/SecondaryButton";

type Props = {
  toggleMobileMenu: () => void;
  showRegisterModal: () => void;
};

const MobileMenu = ({ toggleMobileMenu, showRegisterModal }: Props) => {
  return (
    <>
      <div className="fixed left-0 top-0 w-full h-full bg-[rgba(0,0,0,0.5)] z-10" onClick={toggleMobileMenu}></div>
      <div className="shadow-2xl w-auto h-[100vh] bg-lr-black fixed z-50 right-0 top-0">
        <div>
          <div className="text-white px-10 mt-5 cursor-pointer hover:text-red-400" onClick={toggleMobileMenu}>
            <AiOutlineClose size={30} />
          </div>
          <ul className="flex flex-col items-center text-white" dir="rtl">
            <a>
              <li className="px-10 mt-10 cursor-pointer hover:text-slate-400">
                <BtnSecondary onClick={showRegisterModal}> Log In / Sign Up </BtnSecondary>
              </li>
            </a>
            <a>
              <li className="px-10 mt-10 cursor-pointer hover:text-slate-400">Country Info</li>
            </a>
            <a>
              <li className="px-10 mt-10 cursor-pointer hover:text-slate-400">Startup Visa Guide</li>
            </a>
            <a>
              <li className="px-10 mt-10 cursor-pointer hover:text-slate-400">Programs List</li>
            </a>
            <a>
              <li className="px-10 mt-10 cursor-pointer hover:text-slate-400">Our Services</li>
            </a>
            <a>
              <li className="px-10 mt-10 cursor-pointer hover:text-slate-400">
                English
                <BsGlobe className="inline mr-1" size={20} />
              </li>
            </a>
          </ul>
        </div>
      </div>
    </>
  );
};

export default MobileMenu;
