import React from "react";
import { AiFillLinkedin, AiFillInstagram, AiFillTwitterCircle, AiOutlineCopyrightCircle } from "react-icons/ai";

const Footer = () => {
  return (
    <>
      <footer className="relative bg-lr-light pt-28 light-gradient-bg">
        <img
          src="/icons/logo.png"
          alt="launch-road logo"
          width={110}
          height={110}
          className="absolute top-5 lg:top-32 left-10 lg:left-36 z-0"
        />
        <img
          src="/imgs/footerPattern.png"
          alt="launch-road logo"
          className="absolute top-[25%] lg:top-32 right-0 lg:right-0 z-0"
        />
        <div className="m-auto flex flex-wrap justify-start lg:justify-center ">
          <div className="mt-10 px-20">
            <ul>
              <li className="font-extrabold text-lg uppercase ">Our Services</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Explore</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Assessment</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Safe apply</li>
            </ul>
          </div>
          <div className="mt-10 px-20">
            <ul>
              <li className="font-extrabold text-lg uppercase ">COMPANY</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">About us</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Contact us</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Support</li>
            </ul>
          </div>
          <div className="mt-10 px-20">
            <ul>
              <li className="font-extrabold text-lg uppercase ">RESOURCES</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Programs list</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Startup visa guide</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Country info</li>
              <li className="mt-[0.88rem] text-lr-light-gray cursor-pointer">Blog</li>
            </ul>
          </div>
        </div>
        <div className="flex justify-center pb-5 pt-14">
          <AiFillLinkedin
            size={50}
            className="mr-5 cursor-pointer transition-all duration-200 hover:scale-90 hover:text-blue-800"
          />
          <AiFillInstagram
            size={50}
            className="mr-5 cursor-pointer transition-all duration-200 hover:scale-90 hover:text-pink-700"
          />
          <AiFillTwitterCircle
            size={50}
            className="mr-5 cursor-pointer transition-all duration-200 hover:scale-90 hover:text-blue-400"
          />
        </div>
        <hr className="max-w-5xl m-auto" />
        <p className="py-5 text-center text-slate-500">
          {" "}
          <AiOutlineCopyrightCircle size={20} className="inline" /> LaunchRoad 2023
        </p>
      </footer>
    </>
  );
};

export default Footer;
