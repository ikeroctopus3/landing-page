import React, { useState } from "react";
import { BsGlobe } from "react-icons/bs";
import { BiMenu } from "react-icons/bi";

import BtnSecondary from "../UI/SecondaryButton";
import MobileMenu from "./MobileMenu";
import Register from "./LoginModal";

const Header = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const [registerModal, setRegisterModal] = useState(false);

  // const handleMobileMenu = () => {
  //   setShowMobileMenu(prevStatus => !prevStatus);
  // };

  return (
    <>
      <header className="bg-lr-black h-[3.375rem] sticky top-0 z-10">
        <nav className="m-auto lg-m-width h-full flex justify-between items-center text-white">
          <div className="cursor-pointer flex">
            <img src="/icons/logo.png" alt="logo" className="rounded-full mr-2" width={50} height={50} />
            <span className="font-bold text-lg leading-none mt-2">
              Launch <br /> Road
            </span>
          </div>
          <span className="lg:hidden cursor-pointer" onClick={() => setShowMobileMenu(prevState => !prevState)}>
            <BiMenu size={40} />
          </span>
          <ul className="hidden lg:flex items-center " dir="rtl">
            <a>
              <li className="pl-3 cursor-pointer hover:text-slate-400">
                <BtnSecondary onClick={() => setRegisterModal(true)}> Log In / Sign Up </BtnSecondary>
              </li>
            </a>
            <a>
              <li className="px-3 cursor-pointer hover:text-slate-400">Country Info</li>
            </a>
            <a>
              <li className="px-3 cursor-pointer hover:text-slate-400">Startup Visa Guide</li>
            </a>
            <a>
              <li className="px-3 cursor-pointer hover:text-slate-400">Programs List</li>
            </a>
            <a>
              <li className="px-3 cursor-pointer hover:text-slate-400">Our Services</li>
            </a>
            <a>
              <li className="px-3 cursor-pointer hover:text-slate-400">
                English
                <BsGlobe className="inline mr-1" size={20} />
              </li>
            </a>
          </ul>
        </nav>
      </header>

      {showMobileMenu && (
        <MobileMenu
          toggleMobileMenu={() => setShowMobileMenu(prevState => !prevState)}
          showRegisterModal={() => {
            setRegisterModal(true);
            setShowMobileMenu(false);
          }}
        />
      )}

      {registerModal && <Register toggleRegisterModal={() => setRegisterModal(false)} />}
      {/* <Register toggleRegisterModal={() => setRegisterModal(false)} /> */}
    </>
  );
};

export default Header;
