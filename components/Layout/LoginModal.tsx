import React from "react";

import { AiOutlineClose } from "react-icons/ai";
import { BiLogoLinkedin, BiLogoApple, BiLogoGoogle, BiLogoFacebook } from "react-icons/bi";

export type Props = {
  toggleRegisterModal: () => void;
};

const Register = ({ toggleRegisterModal }: Props) => {
  return (
    <>
      <div className="bg-black opacity-25 fixed w-full h-full left-0 top-0 z-20"></div>
      <div className="bg-white w-[90%]  md:w-[605px] max-h-[95vh] overflow-auto rounded-[30px] fixed left-[50%] top-[3.5rem] -translate-x-2/4 z-30">
        <div onClick={toggleRegisterModal} className=" cursor-pointer mr-5 mt-4 text-center" dir="rtl">
          <AiOutlineClose size={20} className="hover:text-red-600" />
        </div>

        <div className="text-center">
          <h2 className="text-3xl font-bold leading-9">Register for free!</h2>
          <p className="text-login-gray text-sm sm:text-base">
            and get access to <span className="text-blue-600 cursor-pointer">all data</span> limitless, timeless.
          </p>
          <input type="text" className="block m-auto w-5/6 py-3 px-5 rounded-2xl border-2 mt-10" placeholder="Email" />
          <input type="text" className="block m-auto w-5/6 p-3 px-5 rounded-2xl border-2 mt-3" placeholder="Password" />
          <button className="block m-auto mt-[1.65rem] w-5/6 p-3 rounded-2xl border-2 border-lr-blue bg-lr-blue text-lg font-medium text-white hover:bg-lr-darker-blue">
            register now
          </button>
        </div>
        <hr className="mt-[2.82rem] w-5/6 mx-auto border-light-gray border-1" />
        <p className="px-5 py-2 text-center w-[10rem] m-auto -mt-5 bg-white">or continue with</p>
        <div className="w-5/6 flex justify-center mt-[1.12rem] m-auto">
          <div className="cursor-pointer bg-blue-100 rounded-2xl border text-[17px] border-blue-700 text-center py-3 w-[14.75rem] mx-1 md:mx-2 hover:bg-blue-200">
            <BiLogoGoogle className="inline mr-1  text-red-800" size={30} />
            Google
          </div>
          <div className="cursor-pointer bg-blue-100 rounded-2xl border text-[17px] border-blue-700 text-center py-3  w-[14.75rem] mx-1 md:mx-2 hover:bg-blue-200">
            <BiLogoFacebook className="inline mr-1 text-blue-800" size={30} />
            Facebook
          </div>
        </div>
        <div className=" w-5/6 flex justify-center mt-[1.12rem] m-auto">
          <div className="cursor-pointer bg-blue-100 rounded-2xl border text-[17px] border-blue-700 text-center py-3 w-[14.75rem] mx-1 md:mx-2 hover:bg-blue-200">
            <BiLogoLinkedin className="inline mr-1 text-blue-800" size={30} />
            Linkedin
          </div>
          <div className="cursor-pointer bg-blue-100 rounded-2xl border text-[17px] border-blue-700 text-center py-3 w-[14.75rem] mx-1 md:mx-2 hover:bg-blue-200">
            <BiLogoApple className="inline mr-1 text-black" size={30} />
            Apple
          </div>
        </div>
        <p className="text-center text-login-gray mt-[2rem] mb-5">
          Already have an account? <span className="text-blue-600 underline cursor-pointer">Sign in</span>
        </p>
      </div>
    </>
  );
};

export default Register;
