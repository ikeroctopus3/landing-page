import Head from "next/head";

import Header from "@/components/Layout/Header";

import LandingImage from "@/components/landingPage/LandingImage";
import HowWeHelp from "@/components/landingPage/HowWeHelp";
import ApplyStepsGif from "@/components/landingPage/ApplyStepsGif";
import ProgramsList from "@/components/landingPage/ProgramsList";
import SuitCountry from "@/components/landingPage/SuitCountry";
import OurStory from "@/components/landingPage/OurStory";
import SuccessfulCases from "@/components/landingPage/SuccessfulCases";
import CustomerFeedback from "@/components/landingPage/CustomerFeedback";
import Footer from "@/components/Layout/Footer";
// import App from "./_app";

export default function Home() {
  return (
    <>
      <Head>
        <title>Lauch Road</title>
        <link rel="icon" href="/icons/logo.png" />
      </Head>

      <Header />
      <main>
        <LandingImage />
        <HowWeHelp />
        <ApplyStepsGif />
        <ProgramsList />
        <SuitCountry />
        <OurStory />
        <SuccessfulCases />
        <CustomerFeedback />
      </main>
      <Footer />
    </>
  );
}
