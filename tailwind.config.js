/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        "lr-black": "rgba(0, 0, 0, 0.85)",
        "lr-gray": "rgba(0, 0, 0, 0.5)",
        "lr-dark-gray": "rgba(111, 111, 111, 1)",
        "lr-light-gray": "rgba(73, 73, 73, 1)",
        "lr-txt-gray": "rgba(87, 87, 87, 1)",
        "lr-blue": "rgba(0, 101, 225, 1)",
        "lr-darker-blue": "rgba(0, 60, 255, 1)",
        "light-gray": "rgba(0, 0, 0, 0.3)",
        "login-gray": "rgba(91, 91, 91, 1)",
      },
      boxShadow: {
        "lr-bs-1": "0px 4px 4px 0px rgba(0, 0, 0, 0.25)",
        "lr-bs-2": "0px 2px 10px 0px rgba(0, 0, 0, 0.2)",
      },
      backgroundImage: {
        "landing-image": "url('/imgs/Back Ground Image.jpg')",
      },
    },
  },
  plugins: [],
};
